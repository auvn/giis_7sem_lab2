function BezierCurvesDrawer() {

    this.draw = function (points) {

        if (points.length == 4) {


            var point = points[0 ];
            var p1x = point.x
            var p1y = point.y;
            var point = points[1];
            var p2x = point.x;
            var p2y = point.y;
            var point = points[2];
            var p3x = point.x;
            var p3y = point.y;
            var point = points[3];
            var p4x = point.x;
            var p4y = point.y;

            for (var t = 0.0; t <= 1.0; t += 0.01) {
                var x = calculateX(t, p1x, p2x, p3x, p4x);
                var y = calculateY(t, p1y, p2y, p3y, p4y);
                this.plot(Math.round(x), Math.round(y));
            }

            WorkPlace.repaint();
        }

        else {
            alert("At least 4 points have to selected drawing curve");
        }

    };

}

BezierCurvesDrawer.prototype = baseDrawer;

function calculateX(t, p1, p2, p3, p4) {

    return (Math.pow(1 - t, 3)) * p1 + 3 * t * (Math.pow(t - 1, 2)) * p2 + 3 * (Math.pow(t, 2)) * (1 - t) * p3 + (Math.pow(t, 3)) * p4;

}
function calculateY(t, p1, p2, p3, p4) {

    return (Math.pow(1 - t, 3)) * p1 + 3 * t * (Math.pow(t - 1, 2)) * p2 + 3 * (Math.pow(t, 2)) * (1 - t) * p3 + (Math.pow(t, 3)) * p4;

}

