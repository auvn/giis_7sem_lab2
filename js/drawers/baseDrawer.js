function BaseDrawer() {

	this.plot = function (x, y) {
		//WorkPlace.drawPixelByCartesian(x, y, "#ffffff");
		WorkPlace.drawPixel(x, y, "#000000");
		console.log("plotting the point x=" + x + ", y=" + y);
	};
	
	this.plotCompl = function (x, y, brightness) {
		var i = 255 - Math.round(255 * brightness);
		var color = "#" + i.toString(16) + i.toString(16) + i.toString(16);
		//WorkPlace.drawPixelByCartesian(x, y, color);
		WorkPlace.drawPixel(x, y, color);
		console.log("plotting the point x=" + x + ", y=" + y + " with b=" + brightness + " color=" + color);
	}

	this.zoom = function (newScale) {
		WorkPlace.changeScale(WorkPlace.scale + newScale);
	};

	this.clear = function () {
		//govnokod

		WorkPlace.create(100,100);
		WorkPlace.repaint();
	};

}

var baseDrawer = new BaseDrawer();