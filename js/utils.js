function Point(x, y) {
    this.x = x;
    this.y = y;
    this.z = 1;
    this.t = 1;
}

function sign(value) {
    if (value < 0) {
        return -1;
    }
    else if (value > 0) {
        return 1;
    }
    else {
        return 0;
    }
}

function fpart(value) {
	return value - Math.floor(value);
}

function rfpart(value) {
	return 1 - fpart(value);
}

var Namespace = {};

Namespace.pixelsCanvas = "pixelsCanvas";
Namespace.debugButton = "debugButton";
Namespace.nextStepButton = "nextStepButton";
Namespace.stopButton = "stopButton";
Namespace.instrumentsPane = "instrumentsPane";