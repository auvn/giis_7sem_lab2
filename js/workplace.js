function Pixel() {
    this.color = "#FFFFFF";
}

var WorkPlace = {};
WorkPlace.workArea = [];
WorkPlace.width = 0;
WorkPlace.height = 0;
WorkPlace.scale = 5;
WorkPlace.centerX = 0;
WorkPlace.centerY = 0;

WorkPlace.translate = function (physPoint) {

    var px = physPoint.x;
    var py = physPoint.y;

	//incorrect Y when scale <= 2

    var rx = parseInt(px / (this.scale + this.offset()));
    var ry = parseInt(py / (this.scale + this.offset()));

	/*
    console.log("Pixel:" + px + " x " + py);
    this.drawPixel(rx, ry, "#FFFFFF");
    this.repaint();
	*/
    //return new Point(rx - this.centerX, this.centerY - ry);
	return new Point(rx, ry);
}

WorkPlace.offset = function () {
    if (this.scale <= 3)
        return 0;
    else
        return 1;
}

WorkPlace.create = function (width, height) {
    delete this.workArea;

    this.workArea = [];

    this.width = parseInt(width);
    this.height = parseInt(height);

    this.centerX = parseInt(this.width / 2);
    this.centerY = parseInt(this.height / 2);

    console.log("Creating work area: " + this.width + "x" + this.height + " and center at: " + this.centerX + "x" + this.centerY);
    for (var i = 0; i < this.width; i++) {
        var widthPixels = [];
        for (var j = 0; j < this.height; j++) {
            var heightPixel = new Pixel();

            widthPixels.push(heightPixel);

            delete heightPixel;
        }
        this.workArea.push(widthPixels);
    }
	
	
}

WorkPlace.showPixels = function (element) {

    var tempHtmlContainer = "";

    for (var i = 0; i < this.height; i++) {
        for (var j = 0; j < this.width; j++) {
            var pixel = this.workArea[j][i]
            tempHtmlContainer += pixel.isEdited == true ? "1 " : "0 "
        }
        tempHtmlContainer += "</br>"
    }
    element.innerHTML = tempHtmlContainer
}

WorkPlace.getPixelColorByCartesian = function (x, y) {
	return this.workArea[this.centerX + x][this.centerY - y].color;
}

WorkPlace.getPixelColor = function (x, y) {
	return this.workArea[x][y].color;
}

//drawing from the top left corner
WorkPlace.drawPixel = function (x, y, color) {
    var heightPixels;
    if (isCorrectIndex(x, this.workArea)) {
        heightPixels = this.workArea[x];
        if (isCorrectIndex(y, heightPixels)) {
            var pixel = heightPixels[y];
            pixel.color = color;
        }
    }
}
//drawing from the center of the pixels matrix
WorkPlace.drawPixelByCartesian = function (x, y, color) {
    this.drawPixel(this.centerX + x, this.centerY - y, color);
}


WorkPlace.drawPixels = function () {
    var canvas = document.getElementById(Namespace.pixelsCanvas)

    var canvasContext = canvas.getContext("2d")

    var offset = this.offset()

    var x = offset
    var y = offset
    var scale = this.scale

    for (var i = 0; i < this.height; i++) {
        for (var j = 0; j < this.width; j++) {
            var pixel = this.workArea[j][i]
            canvasContext.fillStyle = pixel.color
            canvasContext.fillRect(x, y, scale, scale)
            x = x + scale + offset
        }
        x = offset
        y = y + scale + offset
    }
}

WorkPlace.repaint = function () {
    this.createCanvas()
    this.drawPixels()
}

WorkPlace.createCanvas = function () {
    var canvas = document.getElementById(Namespace.pixelsCanvas)

    var offset = this.offset()

    var commonScale = (this.scale + offset)

    canvas.width = this.width * commonScale + offset
    canvas.height = this.height * commonScale + offset
    canvas.hidden = false

}

WorkPlace.createNewWorkPlace = function () {
    var widthInput = document.getElementById("workPlaceWidth")
    var heightInput = document.getElementById("workPlaceHeight")

    this.create(widthInput.value, heightInput.value)

    var changeScaleButton = document.getElementById("changeScaleButton")
    changeScaleButton.disabled = false

    this.drawPixel(5, 5, "#00ff00")

    this.repaint()

}

WorkPlace.changeScale = function (newScale) {
	if (newScale >= 1 && newScale <= 20) {
		this.scale = parseInt(newScale)
		this.repaint()
	}
}

function isCorrectIndex(index, array) {
    var arrayLen = array.length;
    if (index < arrayLen && index >= 0)
        return true;
    return false;
}