var lineDrawCtrl = {};

var cdaDrawer = new CDALineDrawer();
var brezDrawer;
var wuDrawer;
var drawer = cdaDrawer;

lineDrawCtrl.draw = function(debug) {
	
	var x1 = parseFloat(document.getElementById("x1").value);
	var y1 = parseFloat(document.getElementById("y1").value);
	var x2 = parseFloat(document.getElementById("x2").value);
	var y2 = parseFloat(document.getElementById("y2").value);

	var p1 = new Point(x1, y1);
	var p2 = new Point(x2, y2);

	if (debug) {
		this.debug = drawer.lineDebug(p1, p2);
		startDebug();
	}
	else {
		drawer.line(p1, p2);
		if (this.debug) {
			stopDebug.call(this);
		}
	}
	
	WorkPlace.repaint();	
}

lineDrawCtrl.selectAlgorithm = function(algName) {
	console.log("selected " + algName + " algorithm");
	switch (algName) {
	case "cda":
		drawer = new CDALineDrawer();
		break;
	case "brez":
		drawer = new BrezLineDrawer();
		break;
	case "wu":
		drawer = new WuLineDrawer();
		break;
	}
}