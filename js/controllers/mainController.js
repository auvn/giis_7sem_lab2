var drawer;
var ctrl;
var canvas;

window.addEventListener('load', function () {
    WorkPlace.create(100, 100);
    WorkPlace.repaint();
}, false);

var previousColor;

document.addEventListener("DOMContentLoaded", function () {
    canvas = document.getElementById(Namespace.pixelsCanvas);
    canvas.addEventListener("mousedown", function (event) {

        //smth going is weird
        var x = event.offsetX;
        var y = event.offsetY;
        x -= canvas.offsetLeft;
        y -= canvas.offsetTop;
        //console.log("Physical coords: x=" + x + " y=" + y);
        var physPoint = new Point(x, y);
        var logPoint = WorkPlace.translate(physPoint);
        console.log(logPoint.x + " and " + logPoint.y);


        ctrl.addPoint(logPoint);

//        switch (event.which) {
//            case 1:
//                document.getElementById("x1").value = logPoint.x;
//                document.getElementById("y1").value = logPoint.y;
//                break;
//            case 2:
//                alert('middle');
//                break;
//            case 3:
//                document.getElementById("x2").value = logPoint.x;
//                document.getElementById("y2").value = logPoint.y;
//                break;
//        }
//        previousColor = WorkPlace.getPixelColor(logPoint.x, logPoint.y);
        //WorkPlace.drawPixelByCartesian(logPoint.x, logPoint.y, "#FF7F7F");
//        if (isDraw)
        WorkPlace.drawPixel(logPoint.x, logPoint.y, "#FF7F7F");
        WorkPlace.repaint();
    }, false);

//    canvas.addEventListener("mouseup", function (event) {
//        var x = event.offsetX;
//        var y = event.offsetY;
//        x -= canvas.offsetLeft;
//        y -= canvas.offsetTop;
//        var physPoint = new Point(x, y);
//        var logPoint = WorkPlace.translate(physPoint);
//
//        //WorkPlace.drawPixelByCartesian(logPoint.x, logPoint.y, previousColor);
//        WorkPlace.drawPixel(logPoint.x, logPoint.y, previousColor);
//        WorkPlace.repaint();
//
//    }, false);

    canvas.addEventListener('contextmenu', function (e) {
        e.preventDefault();
    }, false);
}, false);


function selectController() {
    ctrl = this;
}

function startDebug() {
    document.getElementById(Namespace.debugButton).disabled = true;
    document.getElementById(Namespace.nextStepButton).style.display = "inline";
    document.getElementById(Namespace.stopButton).style.display = "inline";
}

function nextStep() {
    if (this.debug.hasNext()) {
        this.debug.nextStep();
        WorkPlace.repaint();
    }
    else {
        stopDebug.call(this);
    }
}

function stopDebug() {
    delete this.debug;
    document.getElementById("debugButton").disabled = false;
    document.getElementById("nextStepButton").style.display = "none";
    document.getElementById("stopButton").style.display = "none";
}

function buildAxis() {
    var xmax = Math.ceil(WorkPlace.width / 2);
    for (var x = -xmax; x <= xmax; x++) {
        WorkPlace.drawPixelByCartesian(x, 0, "#40007f");
    }

    var ymax = Math.ceil(WorkPlace.width / 2);
    for (var y = -ymax; y <= ymax; y++) {
        WorkPlace.drawPixelByCartesian(0, y, "#40007f");
    }

    WorkPlace.repaint();
}

function selectInstruments(htmlFile) {


}